package models;

public class RegisterModel {
    private AccountModel account;

    public AccountModel getAccount() {
        return account;
    }


    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public RegisterModel() {

    }

    public RegisterModel(String prenume,String nume, String email, String parola) {
        AccountModel ac = new AccountModel();
        ac.setPrenume(prenume);
        ac.setNume(nume);
        ac.setEmail(email);
        ac.setParola(parola);
        this.account = ac;
    }
}
