package models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginModel {
    private AccountModel account;

    public AccountModel getAccount() {
        return account;
    }

    @XmlElement
    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public LoginModel(){

    }
    public LoginModel(String email, String password){
        AccountModel ac = new AccountModel();
        ac.setEmail(email);
        ac.setPassword(password);
        this.account = ac;
    }


}
