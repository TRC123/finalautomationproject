package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class AddressPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[1]/div[1]/input")
    WebElement alias;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[2]/div[1]/input")
    WebElement prenume;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[3]/div[1]/input")
    WebElement numeDeFamilie;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[4]/div[1]/input")
    WebElement firma;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[5]/div[1]/input")
    WebElement cif;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[6]/div[1]/input")
    WebElement addressLine1;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[7]/div[1]/input")
    WebElement addressLine2;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[8]/div[1]/input")
    WebElement codPostal;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[9]/div[1]/input")
    WebElement localitate;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[10]/div[1]/select")
    WebElement tara;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/section/div[11]/div[1]/input")
    WebElement telefon;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/div/form/footer/button")
    WebElement newAddressSaveButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"notifications\"]/div/article/ul/li")
    WebElement successMessage;

    @FindBy(how = How.ID, using = "addresses-link")
    WebElement existingAddress;

    @FindBy(how = How.XPATH, using = "//*[contains(text(), 'Sterge')]")
    WebElement deleteAddressButton;

    public AddressPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait((driver), 15);
        PageFactory.initElements(this.driver, this);
    }

    public void addAddress() {
        wait.until(ExpectedConditions.elementToBeClickable(alias));
        alias.click();
        alias.clear();
        alias.sendKeys("O lume noua");

        wait.until(ExpectedConditions.elementToBeClickable(prenume));
        prenume.click();
        prenume.clear();
        prenume.sendKeys("Daniel");

        wait.until(ExpectedConditions.elementToBeClickable(numeDeFamilie));
        numeDeFamilie.click();
        numeDeFamilie.clear();
        numeDeFamilie.sendKeys("Preafericitul");

        wait.until(ExpectedConditions.elementToBeClickable(firma));
        firma.click();
        firma.clear();
        firma.sendKeys("BOR");

        wait.until(ExpectedConditions.elementToBeClickable(addressLine1));
        addressLine1.click();
        addressLine1.clear();
        addressLine1.sendKeys("Calea 13 Septembrie nr 4-60");

        wait.until(ExpectedConditions.elementToBeClickable(addressLine2));
        addressLine2.click();
        addressLine2.clear();
        addressLine2.sendKeys("București");

        wait.until(ExpectedConditions.elementToBeClickable(codPostal));
        codPostal.click();
        codPostal.clear();
        codPostal.sendKeys("050712");

        wait.until(ExpectedConditions.elementToBeClickable(localitate));
        localitate.click();
        localitate.clear();
        localitate.sendKeys("Bucuresti");

        wait.until(ExpectedConditions.elementToBeClickable(tara));
        Actions actions = new Actions(driver);
        actions.moveToElement(tara).build().perform();
        tara.click();
        tara.sendKeys("România");

        wait.until(ExpectedConditions.elementToBeClickable(telefon));
        telefon.click();
        telefon.clear();
        telefon.sendKeys("+40214068279");

        wait.until(ExpectedConditions.elementToBeClickable(newAddressSaveButton));
        newAddressSaveButton.submit();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"notifications\"]/div/article/ul/li")));
        Assert.assertEquals(successMessage.getText(), "Adresa adaugata cu succes!");

    }

    public void cleanUpAddress() {
        Assert.assertEquals(driver.getCurrentUrl(), "http://www.elfast.ro/adrese");
        wait.until(ExpectedConditions.elementToBeClickable(deleteAddressButton));
        deleteAddressButton.click();
    }
}

