package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LoginPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login-form\"]/section/div[1]/div[1]/input")
    WebElement emailIinput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login-form\"]/section/div[2]/div[1]/div/input")
    WebElement pwInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login-form\"]/footer/button")
    WebElement submitButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/section/div/ul/li")
    WebElement invalidCredentialsError;

    @FindBy(how = How.XPATH, using = "//*[@id=\"_desktop_user_info\"]/div/a[1]")
    WebElement logoutbutton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"_desktop_user_info\"]/div/a/span")
    WebElement loginbutton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"_desktop_user_info\"]/div/a[2]/span")
    WebElement loggedUser;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login-form\"]/section/div[1]/div[1]/div/ul/li")
    WebElement emailError;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait((driver), 15);
        PageFactory.initElements(this.driver, this);
    }

    public void quickLogin(){
        emailIinput.click();
        emailIinput.clear();
        wait.until(ExpectedConditions.elementToBeClickable(emailIinput));
        emailIinput.sendKeys("omul@banana.ro");

        pwInput.click();
        pwInput.clear();
        wait.until(ExpectedConditions.elementToBeClickable(pwInput));
        pwInput.sendKeys("Omulbanana1");

        submitButton.submit();
        Assert.assertEquals(loggedUser.getText(), "Omul Banana");
    }

    public void login(String userEmail, String userPw) {
        emailIinput.click();
        emailIinput.clear();
        wait.until(ExpectedConditions.elementToBeClickable(emailIinput));
        emailIinput.sendKeys(userEmail);

        pwInput.click();
        pwInput.clear();
        wait.until(ExpectedConditions.elementToBeClickable(pwInput));
        pwInput.sendKeys(userPw);

        submitButton.submit();
        Assert.assertEquals(loggedUser.getText(), "Omul Banana");

        System.out.println("Currently logged in as: " + loggedUser.getText());
    }

    public void negativeLogin(String userEmail, String userPw) {
        emailIinput.click();
        emailIinput.clear();
        wait.until(ExpectedConditions.elementToBeClickable(emailIinput));
        emailIinput.sendKeys(userEmail);

        pwInput.click();
        pwInput.clear();
        wait.until(ExpectedConditions.elementToBeClickable(pwInput));
        pwInput.sendKeys(userPw);

        submitButton.submit();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"login-form\"]/section/div[1]/div[1]/div/ul/li")   ));
        Assert.assertEquals(emailError.getText(), "Format nevalid.");

//        System.out.println("Currently logged in as: " + loggedUser.getText());
//        wait.until(ExpectedConditions.elementToBeClickable((logoutbutton)));
//        logoutbutton.click();

    }

    public void openLoginPage(String hostname) {
        driver.get(hostname);
        wait.until(ExpectedConditions.elementToBeClickable(loginbutton));
        loginbutton.click();
    }

}

