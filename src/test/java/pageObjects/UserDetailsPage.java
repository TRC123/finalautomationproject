package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class UserDetailsPage {
    private WebDriver driver;
    private Actions actions;
    WebDriverWait wait;

// -------------------------------- Contul tau section ----------------------------------

    @FindBy(how = How.XPATH, using = "//*[contains(text(), 'Informatii')]")
    WebElement informatiiMenu;

    @FindBy(how = How.ID, using = "address-link")
    WebElement adresaMenu;

    @FindBy(how = How.XPATH, using = "//*[@id=\"history-link\"]/span")
    WebElement istoricComenzi;

    @FindBy(how = How.XPATH, using = "//*[@id=\"order-slips-link\"]/span")
    WebElement noteCredit;

    @FindBy(how = How.XPATH, using = "//*[@id=\"emailsalerts\"]/span/i")
    WebElement alerte;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'gdpr')]")
    WebElement gdpr;

// -------------------------------- Informatii tab ----------------------------------

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[2]/div[1]/input")
    WebElement fieldPrenume;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[3]/div[1]/input")
    WebElement fieldName;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[4]/div[1]/input")
    WebElement fieldEmail;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[5]/div[1]/div/input")
    WebElement fieldPasswordInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[6]/div[1]/div/input")
    WebElement fieldNewPasswordInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/footer/button")
    WebElement saveButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[9]/div[1]/span/input")
    WebElement checkbox;

// -------------------------------- GDPR tab ----------------------------------

    @FindBy(how = How.ID, using = "exportDataToPdf")
    WebElement downloadPDF;

// -------------------------------- Methods section ---------------------------

    @FindBy(how = How.XPATH, using = "//*[@id=\"_desktop_cart\"]/div/div/a/span[1]")
    WebElement cos;

    @FindBy(how = How.CLASS_NAME, using = "product-line-grid-body col-md-4 col-xs-8")
    WebElement itemAdded;

    @FindBy(how = How.CLASS_NAME, using = "remove-from-cart")
    WebElement deleteEntry;

    @FindBy(how = How.CLASS_NAME, using = "no-items")
    WebElement noItemsText;

    @FindBy(how = How.CLASS_NAME, using = "blockcart cart-preview inactive")
    WebElement cosgol;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div/div[2]/div/div[2]/div/a")
    WebElement finalizeazaComanda;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div/div[2]/div/div[2]/div[1]/text()")
    WebElement orderMessage;

    @FindBy(how = How.ID, using = "addresses-link")
    WebElement existingAddress;

    public UserDetailsPage(WebDriver driver) {
        this.driver = driver;
        this.actions = new Actions(driver);
        wait = new WebDriverWait((driver), 15);
        PageFactory.initElements(this.driver, this);
    }

    public void goToAddressMenu() {
        if (driver.findElements(By.id("address-link")).size() != 0) {
            wait.until(ExpectedConditions.elementToBeClickable(adresaMenu));
            adresaMenu.click();
        } else {
            wait.until(ExpectedConditions.elementToBeClickable(By.id("addresses-link")));
            existingAddress.click();
        }
    }

    public void accessInformatii() {
        wait.until(ExpectedConditions.elementToBeClickable(informatiiMenu));
        informatiiMenu.click();
        Assert.assertEquals(driver.getCurrentUrl(), "http://www.elfast.ro/identitate");

    }

    public void updateDate(String newUser, String newFamilyName, String newEmail, String password, String newPassword) {
        wait.until(ExpectedConditions.elementToBeClickable(fieldPrenume));
        fieldPrenume.click();
        fieldPrenume.clear();
        fieldPrenume.sendKeys(newUser);

        fieldName.click();
        fieldName.clear();
        fieldName.sendKeys(newFamilyName);

        fieldPasswordInput.click();
        fieldPasswordInput.sendKeys(password);

        checkbox.click();
        saveButton.click();
        Assert.assertEquals(fieldPrenume.getAttribute("value"), newUser);
        Assert.assertEquals(fieldName.getAttribute("value"), newFamilyName);
    }

    public void restoreUserData() {
        fieldPrenume.click();
        fieldPrenume.clear();
        fieldPrenume.sendKeys("Omul");

        fieldName.click();
        fieldName.clear();
        fieldName.sendKeys("Banana");

        fieldPasswordInput.click();
        fieldPasswordInput.sendKeys("Omulbanana1");

        checkbox.click();
        saveButton.click();
    }

    public void downloadGDPR() {
        gdpr.click();
        Assert.assertEquals(driver.getCurrentUrl(), "http://www.elfast.ro/module/psgdpr/gdpr");
        actions.moveToElement(downloadPDF).build().perform();
        downloadPDF.click();
    }

    public void removeFromCart() {
        if (driver.findElements((By.cssSelector("#_desktop_cart .inactive"))).size() == 0) {
            cos.click();
            while (driver.findElements(By.className("remove-from-cart")).size() >= 1) {
                Assert.assertEquals(driver.getCurrentUrl(), "http://www.elfast.ro/cos?action=show");
                wait.until(ExpectedConditions.elementToBeClickable(deleteEntry));
                deleteEntry.click();
                driver.navigate().refresh();
                System.out.println("Obiecte in cos: " + driver.findElements(By.className("remove-from-cart")).size());

            }
            Assert.assertEquals(noItemsText.getText(), "Nu mai sunt produse in cosul tau");
        } else {
            driver.navigate().refresh();
            driver.get("http://www.elfast.ro/cos?action=show");
            System.out.println("Nu mai sunt obiecte in cos");
            Assert.assertEquals(noItemsText.getText(), "Nu mai sunt produse in cosul tau");
        }
    }

    public void removeSingleItem() {
        cos.click();
        Assert.assertEquals(driver.getCurrentUrl(), "http://www.elfast.ro/cos?action=show");
        System.out.println("Obiecte in cos: " + driver.findElements(By.className("remove-from-cart")).size());
        deleteEntry.click();
        driver.navigate().refresh();
        System.out.println("Obiecte in cos: " + driver.findElements(By.className("remove-from-cart")).size());
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"main\"]/div/div[1]/div/div[2]/span"))).isDisplayed();
        Assert.assertEquals(noItemsText.getText(), "Nu mai sunt produse in cosul tau");
        finalizeazaComanda.click();


    }
}

