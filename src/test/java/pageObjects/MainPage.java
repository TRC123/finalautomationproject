package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.SeleniumUtils;

import java.util.List;

public class MainPage {
    private WebDriver driver;
    private Actions actions;
    WebDriverWait wait;

// -------------------------------- Landing page --------------------------------

    @FindBy(how = How.XPATH, using = "//*[@id='search_widget']/form/input[2]")
    WebElement searchBox;

    @FindBy(how = How.XPATH, using = "//*[@id=\"search_widget\"]/form/button")
    WebElement searchButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"category-101\"]/a")
    WebElement toateProduseleMenu;

    @FindBy(how = How.XPATH, using = "//*[@id=\"lnk-noutati\"]/a")
    WebElement noutatiMenu;

    @FindBy(how = How.XPATH, using = "//*[@id=\"category-63\"]/a")
    WebElement retelistica;

// -------------------------------- Item details page --------------------------------

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[1]/div/span[3]/button[1]/i")
    WebElement clickIncrease;

    @FindBy(how = How.XPATH, using = "//*[contains(text(), '7.5 m')]")
    WebElement lungime75m;

    @FindBy(how = How.ID, using = "_desktop_logo")
    WebElement homepage;

// -------------------------------- Order details page --------------------------------

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div/div[2]/div/div[2]/div/a")
    WebElement finalizeazaComandaButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"checkout-addresses-step\"]/div/div/form/div[2]/button")
    WebElement continua;

    @FindBy(how = How.XPATH, using = "//*[@id=\"js-delivery\"]/button")
    WebElement continua2;

    @FindBy(how = How.XPATH, using = "//*[@id=\"payment-option-2-container\"]/label/span")
    WebElement ramburs;

    @FindBy(how = How.XPATH, using = "//*[@id=\"conditions_to_approve[terms-and-conditions]\"]")
    WebElement termsAndConditions;

    @FindBy(how = How.XPATH, using = "//*[@id=\"payment-confirmation\"]/div[1]/button")
    WebElement finalStep;


    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.actions = new Actions(driver);
        wait = new WebDriverWait((driver), 15);
        PageFactory.initElements(this.driver, this);
    }

    public void searchForValue(String searchValue) {
        searchBox.sendKeys(searchValue);
        searchButton.submit();
        String searchfield = driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")).getAttribute("value");
        System.out.println("Searched for: " + searchfield);
        Assert.assertEquals(searchfield, searchValue);
    }

    public void SelectMenuEntryCabluriHDMI(WebDriver driver) {
        wait.until(ExpectedConditions.elementToBeClickable(toateProduseleMenu));
        Actions actions = new Actions(driver);
        actions.moveToElement(toateProduseleMenu).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(retelistica));
        retelistica.click();

        String menuName = driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/h1")).getText();
        Assert.assertEquals(menuName, "PATCH CORD CAT.5E");
        System.out.println("Clicked on menu: " + menuName);
        totalNumberOfItems();

        actions.moveToElement(lungime75m).build().perform();
        lungime75m.click();
        totalNumberOfItems();

        driver.get("http://www.elfast.ro/patch-cord-cat5e/985-187-patch-cord-75m-albastru-cat5e.html#/14-culoare-albastru");
        clickIncrease.click();
        clickIncrease.click();
        clickIncrease.click();

        SeleniumUtils.clickOnStaleElement(driver, By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[2]/button"));

        driver.switchTo().activeElement();
        WebElement finalizareComanda = SeleniumUtils.waitForElementToBeClickable(driver, By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/a"), 15);
        finalizareComanda.click();
    }

    public void orderItem() {
        wait.until(ExpectedConditions.elementToBeClickable(finalizeazaComandaButton));
        finalizeazaComandaButton.click();

        Actions actions = new Actions(driver);
        actions.moveToElement(continua).build().perform();
        continua.click();

        actions.moveToElement(continua2).build().perform();
        continua2.click();

        actions.moveToElement(ramburs).build().perform();
        ramburs.click();

        termsAndConditions.click();

//        finalStep.click();
    }

    public String totalNumberOfItems() {
        List<WebElement> data = driver.findElements(By.xpath("//*[@id=\"js-product-list\"]/div[1]/article"));
        System.out.println("Numarul de rezultate gasite pe pagina curenta este: " + data.size());
        return String.valueOf(data.size());
    }

    public void navigateHomepage() {
        wait.until(ExpectedConditions.elementToBeClickable(homepage));
        homepage.click();
    }
}
