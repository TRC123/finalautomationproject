package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage extends LoginPage {

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.XPATH, using = "//*[@id=\"content\"]/div/a")
    WebElement registrationTab;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[2]/div[1]/input")
    WebElement prenumeInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[3]/div[1]/input")
    WebElement numeInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[4]/div[1]/input")
    WebElement emailInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[5]/div[1]/div/input")
    WebElement parolaInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/section/div[8]/div[1]/span/input")
    WebElement termenisiconditii;

    @FindBy(how = How.XPATH, using = "//*[@id=\"customer-form\"]/footer/button")
    WebElement salveaza;


    public RegistrationPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait((driver), 15);
        PageFactory.initElements(this.driver, this);
    }

    public void openRegistrationpage(String hostname) {
        openLoginPage(hostname);
        wait.until(ExpectedConditions.elementToBeClickable(registrationTab));
        registrationTab.click();
    }

    public void registerAccount(String prenume, String nume, String email, String parola) {
        prenumeInput.click();
        prenumeInput.clear();
        prenumeInput.sendKeys(prenume);

        numeInput.click();
        numeInput.clear();
        numeInput.sendKeys(nume);

        emailInput.click();
        emailInput.clear();
        emailInput.sendKeys(email);

        parolaInput.click();
        parolaInput.clear();
        parolaInput.sendKeys(parola);

        termenisiconditii.click();
//        To be un-commented when final project is reviewed.
//        salveaza.click();
    }
}
