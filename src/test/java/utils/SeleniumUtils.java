package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.HashMap;
import java.util.Map;

public class SeleniumUtils {
    static String downloadPath = "C:\\Users\\CatalinTroaca\\IdeaProjects\\FinalAutomationProject\\src\\test\\resources\\downloaded";
    public static String screenshotPath = "src\\test\\resources\\screenshots";

    public static WebDriver getDriver(String browserType) {
        WebDriver driver = null;
        Browsers browsers = getBrowserEnumFromString(browserType);
        switch (browsers) {
            case CHROME:
                WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                Map<String, Object> preferences = new HashMap<String, Object>();
                preferences.put("download.default_directory", downloadPath);
                chromeOptions.setExperimentalOption("prefs", preferences);
                chromeOptions.addArguments("--start-maximized");
                driver = new ChromeDriver(chromeOptions);
                break;
            case FIREFOX:
                WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
                FirefoxProfile profile = new FirefoxProfile();
                break;
            default:
                System.out.println("WARNING selected browser is not supported: " + browsers.toString());


        }
        return driver;
    }

    public static Browsers getBrowserEnumFromString(String browserType) {
        for (Browsers browser : Browsers.values()) {
            if (browserType.equalsIgnoreCase(browser.toString()))
                return browser;
        }
        System.out.println("Browser not found on supported list");
        return null;
    }

    public static WebElement waitForGenericElement(WebDriver driver, By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by)
        );
    }

    public static WebElement waitForElementToBeClickable(WebDriver driver, By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        return wait.until(
                ExpectedConditions.elementToBeClickable(by)
        );
    }

    public static void clickOnStaleElement(WebDriver driver, By by) {
        int i = 0;
        while (i < 3) {
            try {
                waitForElementToBeClickable(driver, by, 15).click();
                i = 10;
            } catch (StaleElementReferenceException e) {
                System.out.println("Stale Element retry to get it");
                i++;
            }
        }
        if (i != 10)
            Assert.fail("Element is not attached to the page document ");
    }

}
