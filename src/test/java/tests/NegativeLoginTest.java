package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.LoginModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class NegativeLoginTest extends BaseUITest {

    /**
     * This test will use invalid credentials provided via a json file and attempt to log the user in.
     * At the end it will validate the error message.
     */

    private void openbrowser(LoginModel lm) {
        LoginPage lp = new LoginPage(driver);
        lp.openLoginPage(hostname);
        lp.negativeLogin(lm.getAccount().getEmail(), lm.getAccount().getPassword());
    }

    @DataProvider(name = "negativelogDp")
    public Iterator<Object[]> jsonDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        ObjectMapper mapper = new ObjectMapper();
        File f = new File("src\\test\\resources\\data\\negativeLoginData.json");
        LoginModel lm = mapper.readValue(f, LoginModel.class);
        dp.add(new Object[]{lm});
        return dp.iterator();
    }

    @Test(dataProvider = "negativelogDp")
    public void negativeLogin(LoginModel lm) {
        openbrowser(lm);

    }
}
