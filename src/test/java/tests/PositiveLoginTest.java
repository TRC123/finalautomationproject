package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.AccountModel;
import models.LoginModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.LoginPage;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * This test will log the user in using credentials provided via a JSON file
 * and will also log the user out after a successful login
 */


public class PositiveLoginTest extends BaseUITest {
    private void openbrowser(LoginModel lm) {
        LoginPage lp = new LoginPage(driver);
        lp.openLoginPage(hostname);
        lp.login(lm.getAccount().getEmail(), lm.getAccount().getPassword());
    }

    @DataProvider(name = "logDp")
    public Iterator<Object[]> jsonDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        ObjectMapper mapper = new ObjectMapper();
        File f = new File("src\\test\\resources\\data\\logindata.json");
        LoginModel lm = mapper.readValue(f, LoginModel.class);
        dp.add(new Object[]{lm});
        return dp.iterator();
    }



    @Test(dataProvider = "logDp")
    public void loginTest(LoginModel lm) {
        openbrowser(lm);
    }


    /** Login test using Collection iterator dp
     *

     @DataProvider(name = "logindp")
     public Iterator<Object[]> loginDp() {
     Collection<Object[]> dp = new ArrayList<Object[]>();
     dp.add(new String[]{"giani@bemveu.ro", "iubescmafia"});
     return dp.iterator();
     }

     @Test(dataProvider = "jsonDp")
     public void loginTest(String email, String password) {
     LoginPage loginPage = new LoginPage(driver);
     loginPage.openLoginPage(hostname);
     loginPage.login(email,password);
     }

      * @return
     */


}


