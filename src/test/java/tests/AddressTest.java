package tests;

import org.testng.annotations.Test;
import pageObjects.AddressPage;
import pageObjects.LoginPage;
import pageObjects.UserDetailsPage;

public class AddressTest extends BaseUITest {

    /**
     * This test will login the user and create a new address
     */

    @Test
    public void addAddress() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        loginPage.quickLogin();

        UserDetailsPage userDetailsPage = new UserDetailsPage(driver);
        userDetailsPage.goToAddressMenu();

        AddressPage addressPage = new AddressPage(driver);
        addressPage.addAddress();
    }

    /**
     * This test will login the user and delete an existing address
     */

    @Test
    public void removeAddress() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        loginPage.quickLogin();

        UserDetailsPage userDetailsPage = new UserDetailsPage(driver);
        userDetailsPage.goToAddressMenu();

        AddressPage addressPage = new AddressPage(driver);
        addressPage.cleanUpAddress();
    }
}
