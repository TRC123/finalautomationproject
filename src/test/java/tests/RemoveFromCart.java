package tests;

import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.UserDetailsPage;

public class RemoveFromCart extends BaseUITest {

    /**
     * This test removes all from cart and asserts that no items are left.
     */

    @Test
    public void removeItems() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        loginPage.quickLogin();

        UserDetailsPage userDetailsPage = new UserDetailsPage(driver);
        userDetailsPage.removeFromCart();
    }


}
