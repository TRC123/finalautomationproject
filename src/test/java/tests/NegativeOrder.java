package tests;

import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.MainPage;
import pageObjects.UserDetailsPage;

public class NegativeOrder extends BaseUITest {

    /**
     *  This test will fail intentionally.
     *  It will add items to the cart, remove them all and attempt to place the order.
     */

    @Test
    public void cannotOrder() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        loginPage.quickLogin();

        MainPage mainPage = new MainPage(driver);
        mainPage.navigateHomepage();
        mainPage.SelectMenuEntryCabluriHDMI(driver);

        UserDetailsPage userDetailsPage = new UserDetailsPage(driver);
        userDetailsPage.removeSingleItem();
    }


}
