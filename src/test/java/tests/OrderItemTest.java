package tests;

import org.testng.annotations.Test;
import pageObjects.AddressPage;
import pageObjects.LoginPage;
import pageObjects.MainPage;
import pageObjects.UserDetailsPage;

public class OrderItemTest extends BaseUITest {
    /**
     * This test will log the user in, add an address , navigate in menus and select items,
     * will add them to the bag and finalize the order without submitting it.
     */

    @Test
    public void orderItems() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        loginPage.quickLogin();

        UserDetailsPage userDetailsPage = new UserDetailsPage(driver);
        userDetailsPage.goToAddressMenu();

        AddressPage addressPage = new AddressPage(driver);
        addressPage.addAddress();

        MainPage mainPage = new MainPage(driver);
        mainPage.SelectMenuEntryCabluriHDMI(driver);
        mainPage.orderItem();
    }
}
