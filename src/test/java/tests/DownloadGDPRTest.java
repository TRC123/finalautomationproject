package tests;

import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.UserDetailsPage;

/**
 * This test will log the user in, and download his gdpr data in a pdf format in a
 * specified folder.
 */

public class DownloadGDPRTest extends BaseUITest {

    @Test
    public void downloadGDPR() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);
        loginPage.quickLogin();
        UserDetailsPage userDetailsPage = new UserDetailsPage(driver);
        userDetailsPage.downloadGDPR();
    }

}
