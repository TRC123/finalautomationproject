package tests;

import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.UserDetailsPage;

public class UpdateUserDetails extends BaseUITest{

    /**
     * This test logs the user in, navigates to the user information screen and updates his details.
     * This test also has a clean-up method embedded in it that undo the changes.
     */

    @Test
    public void updateUser(){
        UserDetailsPage page2 = new UserDetailsPage(driver);
        LoginPage page1 = new LoginPage(driver);
        page1.openLoginPage(hostname);
        page1.quickLogin();
        page2.accessInformatii();
        page2.updateDate("Gigi","Muschi", "","Omulbanana1","");
        page2.restoreUserData();
    }
}
