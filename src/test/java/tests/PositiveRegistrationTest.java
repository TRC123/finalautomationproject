package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.RegisterModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.RegistrationPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * This test will perform the registration process. Please note that there are some lines that need to be un-commented
 * when the final review is performed.
 */
public class PositiveRegistrationTest extends BaseUITest {

    private void register(RegisterModel rm){
        RegistrationPage rp = new RegistrationPage(driver);
        rp.openRegistrationpage(hostname);
        rp.registerAccount(rm.getAccount().getPrenume(),rm.getAccount().getNume(),rm.getAccount().getEmail(),rm.getAccount().getParola());
    }

    @DataProvider(name = "regDp")
    public Iterator<Object[]> jsonDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        ObjectMapper mapper = new ObjectMapper();
        File f = new File("src\\test\\resources\\data\\registerdata.json");
        RegisterModel rm = mapper.readValue(f, RegisterModel.class);
        dp.add(new Object[]{rm});
        return dp.iterator();
    }

    @Test(dataProvider = "regDp")
    public void registerTest(RegisterModel rm){
    register(rm);
    // Please also see comment in RegistrationPage -> registerAccount
    }

}
