package tests;

import models.SearchModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.MainPage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


/**
 * This test will search for a user added value and display the number of results present in the page.
 */

public class SearchItemTest extends BaseUITest {

    @DataProvider(name = "db")
    public Iterator<Object[]> sqlDpCollection() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://" + dbHostname + ":" + dbPort + "/" + dbSchema, dbUsername, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("SELECT * FROM finalproject.searchvalue;");
            while (results.next()){
                SearchModel sm = new SearchModel();
                sm.setSearchValue(results.getString("searchValue"));
                dp.add(new Object[]{sm});
            }
            connection.close();
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return dp.iterator();
    }

    public void search(SearchModel sm){
        MainPage mainPage = new MainPage(driver);
        mainPage.searchForValue(sm.getSearchValue());
    }

    @Test(dataProvider = "db")
    public void searchItem(SearchModel sm) {
        driver.get(hostname);
        MainPage mainPage = new MainPage(driver);
        search(sm);
        mainPage.totalNumberOfItems();
    }
}
