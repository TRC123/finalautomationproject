package tests;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import utils.OtherUtils;
import utils.SeleniumUtils;

import java.util.Properties;

public class BaseUITest {
    WebDriver driver;
    String hostname;
    String dbHostname;
    String dbPort;
    String dbSchema;
    String dbUsername;
    String dbPassword;
    ExtentReports extent = new ExtentReports();
    ExtentSparkReporter spark = new ExtentSparkReporter("test-results/ExtendReports");
    ExtentTest test;

    @BeforeClass
    public void setUp() {
        String browserType;
        browserType = System.getProperty("browser");

        try {
            Properties prop = OtherUtils.readPropertiesFile("src\\test\\java\\framework.properties");
            if (browserType == null)
                browserType = prop.getProperty("browser");
            System.out.println("Run test with browser:" + browserType);

            driver = SeleniumUtils.getDriver(browserType);
            hostname = prop.getProperty("hostname");
            System.out.println("Use the next hostname:" + hostname);
            dbHostname = prop.getProperty("dbHostname");
            System.out.println("Using DB hostname: " + dbHostname);
            dbPort = prop.getProperty("dbPort");
            System.out.println("Using DB port: " + dbPort);
            dbSchema = prop.getProperty("dbSchema");
            System.out.println("Using DB schema: " + dbSchema);
            dbUsername = prop.getProperty("dbUser");
            dbPassword = prop.getProperty("dbPassword");
            System.out.println("Using DB credentials: " + dbUsername + " / " + dbPassword);

            extent.attachReporter(spark);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @AfterClass
    public void cleanUp() {
        System.out.println("Close driver at end of class test");
        driver.quit();
    }

    @AfterMethod
    public void flush() {
        extent.flush();
    }

    public void logInfoStatus(String message) {
        if (test != null)
            test.log(Status.INFO, message);
        System.out.println(message);
    }
}
